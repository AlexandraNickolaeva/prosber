package dz3.zad4;

import java.util.ArrayList;
import java.util.List;

public class Check {
    public static void main(String[] args) {
        List<Class<?>> result = myMethod(D.class);
        for (Class<?> cls : result) {
            System.out.println(cls.getName());
        }
    }

    public static List<Class<?>> myMethod(Class<?> cls) {
        List<Class<?>> interfaces = new ArrayList<>();
        while (cls != Object.class) {
            for (Class<?> res : cls.getInterfaces()) {
                interfaces.add(res);
                Class<?>[] inter = res.getInterfaces();
                while (inter.length > 0) {
                    for (Class<?> elem : inter) {
                        res = elem;
                        interfaces.add(res);
                        inter = res.getInterfaces();
                    }
                }
            }
            cls = cls.getSuperclass();
        }
        return interfaces;
    }
}
