package dz3.zad2;

public class MyClassReflection {
    public static void main(String[] args) {
        printIsLike(MyClass.class);
    }

    public static void printIsLike(Class<?> cls) {
        if (!cls.isAnnotationPresent(IsLike.class)) {
            return;
        }
        IsLike isLike = cls.getAnnotation(IsLike.class);
        System.out.println("Имя: " + isLike.name());
        System.out.println("Номер: " + isLike.number());
    }
}
