package dz3.dop1;

public class MyClass {
    public static void main(String[] args) {
        String s0 = "()(())";
        String s1 = "()()()";
        String s2 = ")(";
        String s3 = "(()";
        String s4 = "(()()())";
        String s5 = "(";
        String s6 = "";
        String s7 = "((()))";
        System.out.println(isCheck(s0));
    }

    public static boolean isCheck(String s) {
        boolean result = false;
        int count = 0;
        if (s == "")
            result = true;
        else {
            char[] array = s.toCharArray();
            if (array.length % 2 == 0 && array[0] == '(' && array[array.length - 1] == ')') {
                for (int i = 0; i < array.length; i++) {
                    if(array[i] == '(')
                        count++;
                    else
                        count--;
                }
                if(count==0)
                    result = true;
            }
        }
        return result;
    }
}

