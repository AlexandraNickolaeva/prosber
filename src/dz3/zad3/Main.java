package dz3.zad3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) {
        try {
            Method m = APrinter.class.getMethod("print", int.class);
            m.invoke(new APrinter(), 10);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.getMessage();
        }
    }
}
