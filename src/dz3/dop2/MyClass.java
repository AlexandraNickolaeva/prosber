package dz3.dop2;

public class MyClass {
    public static void main(String[] args) {
        String s1 = "";
        String s2 = "{()[]()}";
        String s3 = "{)(}";
        String s4 = "[}";
        String s5 = "[{(){}}][()]{}";
        String s6 = "[(]{}";
        System.out.println(isCheck(s2));
    }

    public static boolean isCheck(String s) {
        boolean result = false;
        int count1 = 0, count2 = 0, count3 = 0;
        if (s == "")
            result = true;
        else {
            char[] array = s.toCharArray();
            if (array.length % 2 == 0 &&
                    (array[0] == '(' || array[0] == '[' || array[0] == '{') &&
                    (array[array.length - 1] == ')' || array[array.length - 1] == ']' || array[array.length - 1] == '}')) {
                for (int i = 0; i < array.length; i++) {
                    switch (array[i]) {
                        case '(' -> count1++;
                        case '[' -> count2++;
                        case '{' -> count3++;
                        case ')' -> count1--;
                        case ']' -> count2--;
                        case '}' -> count3--;
                    }
                }
                if (count1 == 0 && count2 == 0 && count3 == 0) {
                    for (int i = 0; i < array.length - 1; i++) {
                        if ((array[i] == '(' && (array[i + 1] == ']' || array[i + 1] == '}')) ||
                                (array[i] == '[' && (array[i + 1] == ')' || array[i + 1] == '}')) ||
                                (array[i] == '{' && (array[i + 1] == ')' || array[i + 1] == ']'))) {
                            result = false;
                            break;
                        } else {
                            result = true;
                        }
                    }
                }
            }
        }
        return result;
    }
}
