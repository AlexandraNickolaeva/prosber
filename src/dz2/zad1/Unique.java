package dz2.zad1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Unique<T> {

    public static void main(String[] args) {
        ArrayList<Integer> list1 = new ArrayList<>();
        list1.add(0);
        list1.add(0);
        list1.add(1);
        list1.add(2);
        list1.add(3);
        list1.add(4);
        list1.add(4);

        System.out.println(list1);
        myMethod(list1);
        System.out.println(myMethod(list1));

        ArrayList<String> list2 = new ArrayList<>();
        list2.add("aa");
        list2.add("aa");
        list2.add("b");
        list2.add("c");
        list2.add("d");
        list2.add("d");

        System.out.println(list2);
        myMethod(list2);
        System.out.println(myMethod(list2));
    }

    public static <T> Set<T> myMethod(ArrayList<T> list) {
        Set<T> set = new HashSet<>();
        for (T elem : list) {
            set.add(elem);
        }
        return set;
    }
}
