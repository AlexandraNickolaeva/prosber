package dz2.dop1;

import java.util.ArrayList;
import java.util.Arrays;

public class MyClass {
    public static void main(String[] args) {
        String[] words = {"the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is", "day"};
        int k = 4;
        System.out.println(Arrays.toString(myMethod(words, k)));
    }

    public static String[] myMethod(String[] words, int k) {
        Arrays.sort(words);
        ArrayList<String> list = new ArrayList<>();

        int count = 1;
        for (int i = 1; i < words.length; i++) {
            if (words[i - 1] == words[i] && i == words.length - 1) {
                count++;
                list.add(count + words[i - 1]);
            } else if (words[i - 1] == words[i])
                count++;
            else {
                list.add(count + words[i - 1]);
                count = 1;
                if (i == words.length - 1) {
                    list.add(count + words[i]);
                }
            }
        }
        for (int i = 0; i < list.size(); i++) {
            for (int j = i; j < list.size(); j++) {
                if (list.get(i).charAt(0) < list.get(j).charAt(0)) {
                    String x = list.get(i);
                    list.set(i, list.get(j));
                    list.set(j, x);
                } else if (list.get(i).charAt(0) == list.get(j).charAt(0) && list.get(i).charAt(1) < list.get(j).charAt(1)) {
                    String x = list.get(i);
                    list.set(i, list.get(j));
                    list.set(j, x);
                }
            }
        }
        //[4the, 3is, 2sunny, 2day]

        String[] res = new String[k];
        for (int i = 0; i < k; i++) {
            res[i] = list.get(i).substring(1);
        }
        return res;
    }
}
