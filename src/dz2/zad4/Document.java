package dz2.zad4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Document {
    public int id;
    public String name;
    public int pageCount;

    public static void main(String[] args) {
        Document document1 = new Document();
        document1.id = 23;
        ArrayList<Document> list = new ArrayList<>();
        list.add(document1);
        System.out.println(list);
        System.out.println(organizeDocuments(list));
    }

    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> res = new HashMap<>();
        for (Document elem : documents) {
            res.put(elem.id, elem);
        }
        return res;
    }
}
