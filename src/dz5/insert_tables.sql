insert into flowers (title_flower, price_flower)
values ('Роза', 100);
insert into flowers (title_flower, price_flower)
values ('Лилия', 50);
insert into flowers (title_flower, price_flower)
values ('Ромашка', 25);

select *
from flowers;

insert into clients (name_client, mobile_client)
values('Анна', 89876751234);
insert into clients (name_client, mobile_client)
values('Игорь', 89871111203);
insert into clients (name_client, mobile_client)
values('Евгений', 89844451203);
insert into clients (name_client, mobile_client)
values('Виктор', 89676751403);
insert into clients (name_client, mobile_client)
values('Дмитрий', 89876756783);

select *
from clients;

insert into orders(id_flower, id_client, count, date_order)
values (1, 1, -5, '2023-01-13');
insert into orders(id_flower, id_client, count, date_order)
values (2, 1, 1001, '2023-01-11');
insert into orders(id_flower, id_client, count, date_order)
values (1, 2, 5, '2023-01-13');
insert into orders(id_flower, id_client, count, date_order)
values (2, 2, 10, '2023-01-12');
insert into orders(id_flower, id_client, count, date_order)
values (3, 2, 1, '2022-01-13');
insert into orders(id_flower, id_client, count, date_order)
values (1, 3, 55, '2022-01-13');
insert into orders(id_flower, id_client, count, date_order)
values (2, 4, 1, '2023-01-15');
insert into orders(id_flower, id_client, count, date_order)
values (3, 5, 57, '2023-01-17');

select *
from orders;
