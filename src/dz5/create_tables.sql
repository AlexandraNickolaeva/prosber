create table flowers
(
    id_flower serial primary key,
    title_flower varchar(30) NOT NULL,
    price_flower integer NOT NULL
);

select *
from flowers;

create table clients
(
    id_client serial primary key,
    name_client varchar(30) NOT NULL,
    mobile_client varchar(11) NOT NULL
);

select *
from clients;

create table orders
(
    id_order serial primary key,
    id_flower integer references flowers(id_flower),
    id_client integer references clients(id_client),
    count integer NOT NULL check(count >= 1 and count <= 1000),
    date_order date
);

select *
from orders;
