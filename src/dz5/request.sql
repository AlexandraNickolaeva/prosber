select id_order Идентификатор_заказа, title_flower Вид_цветов, count Количество_цветов,
       date_order Дата_заказа, name_client Имя_клиента, mobile_client Номер_телефона_клиента
from orders join clients on orders.id_client = clients.id_client
            join flowers on orders.id_flower = flowers.id_flower
where id_order = 2;

select id_order Идентификатор_заказа, title_flower Вид_цветов, count Количество_цветов,
       date_order Дата_заказа, orders.id_client Идентификатор_клиента,name_client Имя_клиента,
       mobile_client Номер_телефона_клиента
from orders join clients on orders.id_client = clients.id_client
            join flowers on orders.id_flower = flowers.id_flower
where clients.id_client = 2 AND date_order>=now() - interval '1mons';

select id_order as "Идентификатор заказа", title_flower as "Название цветов", count "Количество цветов"
from orders join flowers on orders.id_flower = flowers.id_flower
where count = (select max(count)
               from orders);

select sum(orders.count * flowers.price_flower) Общая_выручка
from flowers join orders on flowers.id_flower = orders.id_flower;
