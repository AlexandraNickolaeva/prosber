package main.java.com.example.demo;

import com.example.demo.dbexample.dao.BookDAO;
import com.example.demo.dbexample.dao.UserDAO;
import com.example.demo.dbexample.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.sql.SQLException;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

    private BookDAO bookDAO;
    private UserDAO userDAO;

    @Autowired
    public void setBookDAO(BookDAO bookDAO) {
        this.bookDAO = bookDAO;
    }

    @Autowired
    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Override
    public void run(String... args) throws SQLException {

        userDAO.insertUser(new User("Маруся", "Клава", "1998-02-01", "81571300078", "hdjd@mail.ru", "Мертвые души, Отцы и дети"));
        userDAO.insertUser(new User("Милка", "Валентина", "2018-02-01", "89679990078", "hdjd@mail.ru", "Анна Каренина, Обломов"));
        userDAO.findBooks("81571300078");
        bookDAO.infoBooks("Обломов");

    }
}

