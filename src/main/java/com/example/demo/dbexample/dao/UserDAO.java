package main.java.com.example.demo.dbexample.dao;

import com.example.demo.dbexample.model.User;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
@Scope("prototype")
public class UserDAO {
    private final Connection connection;
    private final BookDAO bookDAO;

    public UserDAO(Connection connection, BookDAO bookDAO) {
        this.connection = connection;
        this.bookDAO = bookDAO;
    }

    public void insertUser(User u) throws SQLException {
        PreparedStatement selectQuery = connection.prepareStatement("insert into client" + "(surname, name_client, birthday, mobile, mail, books_client)" + "values(?, ?, ?, ?,?,?)");

        selectQuery.setString(1, u.getSurname());
        selectQuery.setString(2, u.getName_client());
        selectQuery.setString(3, u.getBirthday());
        selectQuery.setString(4, u.getMobile());
        selectQuery.setString(5, u.getMail());
        selectQuery.setString(6, u.getBooks_client());

        // ResultSet resultSet = selectQuery.executeQuery();
    }

    public void findBooks(String mobile) throws SQLException {
        PreparedStatement selectQuery = connection.prepareStatement("select books_client from client where mobile = ?");
        selectQuery.setString(1, mobile);
        ResultSet resultSet = selectQuery.executeQuery();
        while (resultSet.next()) {
            String result = resultSet.getString("books_client");
            String[] title = result.split(", ");
            for (String qwe : title)
                bookDAO.infoBooks(qwe);
        }
    }
}