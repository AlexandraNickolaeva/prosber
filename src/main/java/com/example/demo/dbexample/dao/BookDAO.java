package main.java.com.example.demo.dbexample.dao;

import com.example.demo.dbexample.model.Book;
import org.springframework.stereotype.Component;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class BookDAO {
    private final Connection connection;

    public BookDAO(Connection connection) {
        this.connection = connection;
    }

    public Book infoBooks(String title) throws SQLException {
        PreparedStatement selectQuery = connection.prepareStatement("select * from book where title = ?");
        selectQuery.setString(1, title);
        ResultSet resultSet = selectQuery.executeQuery();
        Book book = new Book();
        while (resultSet.next()) {
            book.setBookAuthor(resultSet.getString("author"));
            book.setBookTitle(resultSet.getString("title"));
            System.out.println(book);
        }
        return book;
    }
}
