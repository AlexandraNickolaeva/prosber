create table client
(
    id_client serial primary key,
    surname varchar(30) NOT NULL,
    name_client varchar(30) NOT NULL,
    birthday varchar(11) NOT NULL,
    mobile varchar(11) NOT NULL,
    mail varchar(30) NOT NULL,
    books_client varchar(100) NOT NULL
);

select * from client;

create table book
(
    id_book serial primary key,
    title varchar(30) NOT NULL,
    author varchar(30) NOT NULL
)

insert into book (title, author)
values('���� � ����', '��������');
insert into book (title, author)
values('���� ��������', '�������');
insert into book (title, author)
values('������� ����', '������');
insert into book (title, author)
values('�������', '��������');

select * from book;