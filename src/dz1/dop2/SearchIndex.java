package dz1.dop2;

import java.util.Scanner;

public class SearchIndex {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int[] mas = new int[n];
        for (int i = 0; i < n; i++) {
            mas[i] = input.nextInt();
        }
        int p = input.nextInt();
        int low = 0;
        int high = n - 1;
        int res = -1;
        while (low <= high) {
            int index = (high - low) / 2 + low;
            if (mas[index] == p) {
                res = index;
                break;
            } else if (mas[index] < p)
                low = index + 1;
            else if (mas[index] > p)
                high = index - 1;
        }
        System.out.println(res);
    }
}
