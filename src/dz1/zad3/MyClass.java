package dz1.zad3;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.Scanner;

public class MyClass {
    private static final String PKG_DIRECTORY = "C:\\Users\\anna\\IdeaProjects\\ProSber\\src\\dz1\\zad3";
    private static final String INPUT_FILE_NAME = "input.txt";
    private static final String OUTPUT_FILE_NAME = "output.txt";

    public static void myMethod() throws Exception {
        Scanner scanner = new Scanner(new File(PKG_DIRECTORY + "\\" + INPUT_FILE_NAME));
        Writer writer = new FileWriter(PKG_DIRECTORY + "\\" + OUTPUT_FILE_NAME);
        try (scanner; writer) {
            while (scanner.hasNext()) {
                writer.write(scanner.nextLine().toUpperCase() + "\n");
            }
        }
    }

    public static void main(String[] args) throws Exception {
        myMethod();
    }
}

