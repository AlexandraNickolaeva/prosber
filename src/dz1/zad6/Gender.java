package dz1.zad6;

public enum Gender {
    Male("Мужской"),
    Female("Женский");

    private String sex;

    Gender(String sex) {
        this.sex = sex;
    }

    public String getSex() {
        return sex;
    }
}
