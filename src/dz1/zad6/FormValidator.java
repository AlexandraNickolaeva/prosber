package dz1.zad6;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FormValidator {
    public void checkName(String str) {
        if (str.length() < 2 || str.length() > 20)
            throw new MyExceptions("Длина имени должна быть от 2 до 20 символов");
        if (Character.isLowerCase(str.charAt(0)))
            throw new MyExceptions("Первая буква имени должна быть заглавной!");
    }

    public void checkBirthdate(String str) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.y");
        Date dateUser = formatter.parse(str);
        Date dateNow = new Date();
        if (dateUser.getYear() < 0) {
            throw new MyExceptions("Дата рождения должна быть не раньше 01.01.1900");
        }
        if (dateUser.after(dateNow)) {
            throw new MyExceptions("Дата рождения должна быть не позже текущей даты");
        }
    }

    public void checkGender(String str) {
        if (!(str.equals(Gender.Male.getSex())) && !(str.equals(Gender.Female.getSex()))) {
            throw new MyExceptions("Некорректный пол. Мужской/Женский");
        }
    }

    public void checkHeight(String str) {
        if (Double.parseDouble(str) < 0)
            throw new MyExceptions("Рост должен быть положительным числом");
    }
}
