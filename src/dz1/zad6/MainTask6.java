package dz1.zad6;

import java.text.ParseException;

public class MainTask6 {
    public static void main(String[] args) throws ParseException {
        FormValidator formValidator = new FormValidator();
        //  formValidator.checkName("a");
        //  formValidator.checkName("aa");
        formValidator.checkName("Aa");
        //  formValidator.checkBirthdate("14.12.1889");
        //  formValidator.checkBirthdate("15.12.2023");
        formValidator.checkBirthdate("15.12.1995");
        // formValidator.checkGender("laLa");
        formValidator.checkGender("Мужской");
        formValidator.checkGender("Женский");
        // formValidator.checkHeight("-123");
        formValidator.checkHeight("0");
        formValidator.checkHeight("150");
    }
}
