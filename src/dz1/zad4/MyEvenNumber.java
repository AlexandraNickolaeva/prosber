package dz1.zad4;

public class MyEvenNumber {
    private int n;

    public MyEvenNumber(int n) {
        if (n % 2 != 0)
            throw new MyException();
        else
            this.n = n;
    }

    public int getN() {
        return n;
    }

    public void setN(int newN) {
        if (newN % 2 != 0)
            throw new MyException();
        else
            this.n = newN;
    }
}
