package dz1.dop1;

import java.util.Arrays;
import java.util.Scanner;

public class Maximum {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] mas = new int[n];
        for (int i = 0; i < n; i++) {
            mas[i] = scanner.nextInt();
        }
        Arrays.sort(mas);
        System.out.println(mas[n-1] + " " + mas[n-2]);
    }
}
