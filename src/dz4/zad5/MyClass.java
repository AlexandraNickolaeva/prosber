package dz4.zad5;

import java.util.List;

public class MyClass {
    public static void main(String[] args) {
        List<String> st = List.of("abc", "dgs", "shs");
        System.out.print(st.stream()
                .map(String::toUpperCase)
                .reduce((x, y) -> x + "," + y)
                .get());
    }
}
