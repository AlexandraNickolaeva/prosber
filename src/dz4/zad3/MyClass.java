package dz4.zad3;

import java.util.List;

public class MyClass {
    public static void main(String[] args) {
        List<String> st = List.of("abc", "", "", "def", "qqq");
        long counter = st.stream().filter(s -> s.length() > 0).count();
        System.out.println(counter);
    }
}
