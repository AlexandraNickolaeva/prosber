package dz4.zad4;

import java.util.Collections;
import java.util.List;

public class MyClass {
    public static void main(String[] args) {
        List<Double> l = List.of(1.2, 0.3, 1.23, 5.8, 3.2);
        l.stream().sorted(Collections.reverseOrder()).forEach(System.out::println);
    }
}
