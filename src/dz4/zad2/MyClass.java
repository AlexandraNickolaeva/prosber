package dz4.zad2;

import java.util.List;

public class MyClass {
    public static void main(String[] args) {
        List<Integer> numbers = List.of(1, 2, 3, 4, 5);
        int mult = numbers.stream()
                .reduce(1, (a, b) -> a * b);
        System.out.println(mult);
    }
}
