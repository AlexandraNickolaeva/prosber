package dz4.zad6;

import java.util.Set;
import java.util.stream.Collectors;

public class MyClass {
    public static void main(String[] args) {
        Set<Integer> set1 = Set.of(11, 22, 33);
        Set<Integer> set2 = Set.of(44, 55, 66);
        Set<Set<Integer>> set = Set.of(set1, set2);

        System.out.println(set.stream()
                .flatMap(Set::stream)
                .collect(Collectors.toSet()));
    }
}
