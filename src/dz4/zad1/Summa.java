package dz4.zad1;

import java.util.stream.IntStream;

public class Summa {
    public static void main(String[] args) {
        int sum = IntStream.range(1, 101)
                .filter(i -> i % 2 == 0)
                .sum();
        System.out.println(sum);
    }
}
